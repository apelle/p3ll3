from django.contrib.auth.models import Group, Permission
from django.db.models import Q

groups = {
	'Ammministratori': {
		'fields': ['autore', 'categoria', 'evento', 'file', 'keyword', 'famiglia', 'user'],
		'permissions': ['add', 'change', 'delete', 'view'],
	},
	'Ospiti': {
		'fields': ['autore', 'categoria', 'evento', 'file', 'keyword'],
		'permissions': ['view'],
	},
	'Lettori': {
		'fields': ['autore', 'categoria', 'evento', 'file', 'keyword'],
		'permissions': ['view'],
	},
	'Autori': {
		'fields': ['autore', 'categoria', 'evento', 'file', 'keyword'],
		'permissions': ['add', 'change', 'delete', 'view'],
	},
}

for k, g in groups.items():
	filters_f = Q()
	for f in g['fields']:
		filters_f.add(Q(codename__contains=f), Q.OR)
	
	filters_p = Q()
	for p in g['permissions']:
		filters_p.add(Q(codename__contains=p), Q.OR)
	
	Group.objects.get_or_create(name=k)[0].permissions.set(Permission.objects.filter(filters_f, filters_p))

